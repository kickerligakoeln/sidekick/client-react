import React from 'react';
import {Route, Switch} from "react-router-dom";
import Loading from "./elements/dumb/Loading/Loading";
import Dashboard from "./elements/modules/Dashboard/Dashboard";

const App: React.FC<React.HTMLAttributes<HTMLDivElement>> = () => {
    return (
            <main>
                <Switch>
                    <Route path={'/'} component={Dashboard}/>
                </Switch>

                <Loading/>
            </main>
    );
}

export default App;
