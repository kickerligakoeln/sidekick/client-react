import React from 'react';
import Location from "../../../js/rest/Location";
import Checkin from "../../../js/rest/Checkin";
import Geo from "../../../js/utils/Geo";
import {ChannelSubscription, PubSub} from "../../../js/pubsub";
import {LocationInterface} from "../../../js/interfaces/LocationInterface";
import {CheckinRequestInterface, CheckinInterface} from "../../../js/interfaces/CheckinRequestInterface";
import {VisibleState} from "../../../js/enums/VisibleState";

import Map from "../../smart/Map/Map";
import Brand from "../../dumb/Brand/Brand";
import ButtonCheckin from "../../dumb/ButtonCheckin/ButtonCheckin";
import ListLocations from "../../dumb/List/Locations/Locations";
import HintActiveCheckins from "../../dumb/HintActiveCheckins/HintActiveCheckins";
import CheckinDetails from "../../dumb/CheckinDetails/CheckinDetails";

import "./Dashboard.scss";

export default class Dashboard extends React.Component<any, any> {

    myChannels: any[] = [];

    mapChannel!: ChannelSubscription;
    checkinChannel!: ChannelSubscription;
    locationChannel!: ChannelSubscription;
    loadingChannel!: ChannelSubscription;

    constructor(props: any) {
        super(props);

        this.mapChannel = PubSub.join('#Map');
        this.checkinChannel = PubSub.join('#Checkin');
        this.locationChannel = PubSub.join('#Locations');
        this.loadingChannel = PubSub.join('#Loading');

        this.clickButton = this.clickButton.bind(this);

        this.state = {
            loading: false
        }
    }

    componentDidMount() {
        this.myChannels = [
            this.mapChannel.on('request-active-checkins', () => this.getActiveCheckins()),
            this.checkinChannel.on('request-checkin', () => this.getRecommendLocations()),
            this.locationChannel.on('choose', (l: LocationInterface) => this.checkin(l))
        ];
    }

    componentWillUnmount() {
        this.mapChannel.unsubscribe(this.myChannels, 'request-active-checkins');
        this.checkinChannel.unsubscribe(this.myChannels, 'request-checkin');
        this.locationChannel.unsubscribe(this.myChannels, 'choose');
    }

    /**
     * fetch active checkins
     */
    getActiveCheckins = () => {
        Checkin.list.active()
            .then(resp => this.mapChannel.send('response-active-checkins', resp));
    }


    /**
     * fetch recommend locations based on current {coords}
     */
    getRecommendLocations = () => {
        Geo.position()
            .then((coords: any) => {
                Location.list.recommendations(coords)
                    .then(resp => this.locationChannel.send('response-recommendations', resp))
                    .catch(e => console.error('no recommendations', e));
            })
            .catch(() => {
                this.loadingChannel.send('isLoaded', {
                    status: false,
                    message: "Sorry, enable your location sharing first. Reload this page!"
                });
            })
    }


    /**
     * do checkin
     * @param l
     */
    checkin = (l: LocationInterface) => {
        this.loadingChannel.send('isLoaded', {
            status: false,
            message: "Activate Heldenruf!"
        });

        let data: CheckinRequestInterface = {
            visible: VisibleState.PUBLIC,
            type: 'other',
            coords: {
                latitude: l.latitude,
                longitude: l.longitude
            },
            location: {
                id: l.url,
            }
        }

        Checkin.add(data).then((checkin: CheckinInterface) => {
            this.checkinChannel.send('response-checkin', checkin);
            this.locationChannel.send('response-recommendations', []);
            this.getActiveCheckins();
        });
    }


    clickButton = () => {
        this.setState({
            loading: true
        });
        this.getRecommendLocations()
    }


    render() {
        return (
            <div className="dashboard">
                <section className="topbar -fixed">
                    <Brand/>
                </section>

                <section className="map">
                    <Map/>
                </section>

                <section className="action-bar">
                    <div className="container">
                        <ButtonCheckin click={this.clickButton} loading={this.state.loading}/>
                        <div className="-spacer"></div>
                        <HintActiveCheckins/>
                    </div>
                    <div className="container">
                        <ListLocations/>
                    </div>
                </section>

                <CheckinDetails />
            </div>
        );
    }

}
