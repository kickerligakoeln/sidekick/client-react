import React from 'react';
import SmartComponent from "../../../js/SmartComponent";
import Geo from "../../../js/utils/Geo";
import {LoadScript, GoogleMap, Marker, MarkerClusterer} from "@react-google-maps/api"
import {Settings} from "../../../js/utils/Settings";
import {ChannelSubscription, PubSub} from "../../../js/pubsub";
import {CheckinInterface} from "../../../js/interfaces/CheckinRequestInterface";
import {GeoInterface} from "../../../js/interfaces/GeoInterface";

import "./map.scss"

/*
    Google Maps Documentation:
    https://react-google-maps-api-docs.netlify.app/#infowindow
 */

export default class Map extends SmartComponent {

    loadingChannel!: ChannelSubscription;
    mapChannel!: ChannelSubscription;
    checkinChannel!: ChannelSubscription;

    iconCheckin: any = {
        me: "/images/checkin_me.png",
        default: "/images/checkin.png"
    }

    constructor(props: any) {
        super(props);

        this.loadingChannel = PubSub.join('#Loading');
        this.mapChannel = PubSub.join('#Map');
        this.checkinChannel = PubSub.join('#Checkin');

        this.state = {
            geoAvailable: false,
            zoom: 7,
            coords: {
                latitude: 50.785020,
                longitude: 10.343540
            }
        };
    }

    componentDidMount() {
        this.loadingChannel.send('isLoaded', {
            status: false,
            message: "Loading active checkins"
        });

        // fetch current coords
        Geo.position().then((coords: GeoInterface) => {
            this.setState({
                geoAvailable: true,
                zoom: 15,
                coords,
            });
        });

        this.mapChannel.send('request-active-checkins', true);
        this.mapChannel.on('response-active-checkins', (checkins: any[]) => {
            this.setState({checkins});
            this.loadingChannel.send('isLoaded', {status: true});
        });
    }


    /**
     * send chosen checkin
     * @param checkin
     */
    clickMarker = (checkin: CheckinInterface) => {
        this.checkinChannel.send('response-details', checkin);
    }


    render() {
        if (!this.state.coords) {
            return null;
        } else {
            const center = {
                lat: this.state.coords.latitude,
                lng: this.state.coords.longitude
            };

            return (
                    <LoadScript id="script-loader"
                                googleMapsApiKey={Settings.get('GOOGLE_API_KEY')}>
                        <GoogleMap
                                id="map"
                                zoom={this.state.zoom}
                                center={center}
                                clickableIcons={false}
                                options={{
                                    fullscreenControl: false,
                                    mapTypeControl: false,
                                    streetViewControl: false
                                }}>

                            {this.state.geoAvailable &&
                            <Marker position={{lat: center.lat, lng: center.lng}} />
                            }

                            {(this.state.checkins) ? (
                                    <MarkerClusterer options={{}}>
                                        {(clusterer) =>
                                                this.state.checkins.map((c: CheckinInterface) => (
                                                        <Marker key={c._id.$oid}
                                                                // label={c.location.id}
                                                                icon={this.iconCheckin.default}
                                                                position={{
                                                                    lat: c.coords.latitude,
                                                                    lng: c.coords.longitude
                                                                }}
                                                                clusterer={clusterer}
                                                                onClick={() => this.clickMarker(c)}
                                                        />
                                                ))
                                        }
                                    </MarkerClusterer>
                            ) : null}

                        </GoogleMap>
                    </LoadScript>
            );
        }
    }
}
