import React from 'react';
import DumbComponent from "../../../js/DumbComponent";

import "./brand.scss"

export default class Brand extends DumbComponent {

    render() {
        return (
                <a href="/" className="brand">Sidekick</a>
        );
    }

}
