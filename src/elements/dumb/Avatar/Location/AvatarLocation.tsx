import React from 'react';
import DumbComponent from "../../../../js/DumbComponent";
import Icon from "../../Icon/Icon";

export default class AvatarLocation extends DumbComponent {

    constructor(props: any) {
        super(props);

        const {isLocation} = props;
        this.state = {
            isLocation
        }
    }

    componentDidUpdate(prevProps: any) {
        if (prevProps.isLocation !== this.props.isLocation) {
            this.setState({isLocation: this.props.isLocation});
        }
    }

    render() {
        return (
            <div className="avatar -location">
                <Icon icon={(this.state.isLocation === 'private') ? 'home' : 'beer'}/>
            </div>
        );
    }

}