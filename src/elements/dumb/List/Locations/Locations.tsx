import React from 'react';
import DumbComponent from "../../../../js/DumbComponent";
import {ChannelSubscription, PubSub} from "../../../../js/pubsub";

import "./locations.scss";
import {LocationInterface} from "../../../../js/interfaces/LocationInterface";
import AvatarLocation from "../../Avatar/Location/AvatarLocation";

export default class ListLocations extends DumbComponent {

    locationChannel!: ChannelSubscription;

    constructor(props: any) {
        super(props);

        this.state = {};

        this.locationChannel = PubSub.join('#Locations');
        this.choose = this.choose.bind(this);
    }

    componentDidMount() {
        this.locationChannel.on('response-recommendations', (locations: any) => {
            this.setState({
                locations
            });
        });
    }

    /**
     * click on location
     * @param l
     */
    choose = (l: any) => {
        this.locationChannel.send('choose', l);
    }

    render() {
        if (!this.state.locations || this.state.locations.length === 0) {
            return null;
        } else {
            return (
                <div className="locations">
                    {this.state.locations.map((l: LocationInterface, index: number) => (
                        <div className="location" key={l.url} onClick={() => this.choose(l)}>
                            <AvatarLocation isLocation={l.url}/>
                            <div className="description">
                                <p>
                                    <strong>{l.name}</strong><br/>
                                    {(!l.street) ? (
                                        <small>latitude: {l.latitude}<br/> longitude: {l.longitude}</small>
                                    ) : (
                                        <span>{l.street}</span>
                                    )}
                                </p>
                            </div>
                            <div className="-spacer"></div>
                            <div className="distance">{Math.round(l.distance)}m</div>
                        </div>
                    ))}
                </div>
            );
        }
    }

}
