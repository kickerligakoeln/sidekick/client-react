import React from "react";
import DumbComponent from "../../../js/DumbComponent";
import {ChannelSubscription, PubSub} from "../../../js/pubsub";

import './loading.scss'
import Icon from "../Icon/Icon";

export default class Loading extends DumbComponent {

    static defaultProps = {
        isLoaded: true
    };

    loadingChannel!: ChannelSubscription;

    constructor(props: any) {
        super(props);
        this.loadingChannel = PubSub.join('#Loading');
    }

    componentDidMount() {
        this.setState({isLoaded: this.props.isLoaded});
        this.loadingChannel.on('isLoaded', (response: any) => {
            this.setState({
                isLoaded: response.status,
                message: response.message || 'LOADING'
            });
        });
    }

    render() {
        if (!this.state) {
            return null;
        } else {
            return (
                    <div className={`loading -fix ${(!this.state.isLoaded) ? '-in' : ''}`}>
                        <Icon icon={'flash'} size={120}/>
                        <p>{this.state.message}</p>

                        <div className="cred">{new Date().getFullYear()}, © <strong>Kölner Kickerliga</strong></div>
                    </div>
            );
        }
    }
}
