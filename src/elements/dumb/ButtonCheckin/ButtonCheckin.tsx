import React from 'react';
import Icon from "../../dumb/Icon/Icon";
import {useTranslation} from "react-i18next/hooks";

const ButtonCheckin = (props: any) => {
    const [t, i18n] = useTranslation();

    return (
            (props.loading) ? (
                    <button className="btn-primary" disabled>
                        <Icon icon={'flash'}/>
                        {t('button.loading')}
                    </button>
            ) : (
                    <button className="btn-primary" onClick={() => props.click()}>
                        <Icon icon={'flash'}/>
                        {i18n.t('button.checkin')}
                    </button>
            )
    )
}


export default ButtonCheckin;
