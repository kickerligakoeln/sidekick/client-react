import React from 'react';
import DumbComponent from "../../../js/DumbComponent";
import AvatarLocation from "../Avatar/Location/AvatarLocation";
import {ChannelSubscription, PubSub} from "../../../js/pubsub";
import {CheckinInterface} from "../../../js/interfaces/CheckinRequestInterface";
import Countdown from "react-countdown";
import {Time} from "../../../js/enums/Time";
import Icon from "../Icon/Icon";

/*
    Countdown Documentation
    https://www.npmjs.com/package/react-countdown
 */

export default class CheckinDetails extends DumbComponent {

    checkinChannel!: ChannelSubscription;

    constructor(props: any) {
        super(props);

        this.checkinChannel = PubSub.join('#Checkin');

        this.state = {};
    }


    componentDidMount() {
        this.checkinChannel.on('response-details', (checkin: CheckinInterface) => {
            this.setState({checkin});
        });
    }


    closeHtml = () => {
        this.setState({checkin: null});
    }


    htmlCheckin = (c: CheckinInterface) => {
        // todo: it could be easier to do: date={'checkinDate' + 1hour}
        let now: any = new Date();
        let checkinDate: any = new Date(c.create);
        let duration: number = Time.CHECKIN_INTERVALL - Math.abs(now - checkinDate);

        return (
            <div className="container">
                <AvatarLocation isLocation={c.location.id}/>

                <div>
                    <p>
                        <strong>{c.location.id}</strong><br/>
                        <small>latitude: {c.coords.latitude} | longitude: {c.coords.longitude}</small>
                    </p>

                    <div className="counter-wrapper">
                        <Countdown
                            date={Date.now() + duration}
                            daysInHours={true}
                            className={"counter"}/>
                    </div>
                </div>

                <div className="-spacer"></div>

                <button className="btn-default" onClick={() => this.closeHtml()}>
                    <Icon icon={'times'} />
                </button>

            </div>
        )
    }


    render() {
        return (
            <section className={`details-bar -fixed ${this.state.checkin ? '-in' : null}`}>
                {this.state.checkin ? this.htmlCheckin(this.state.checkin) : null}
            </section>
        );
    }

}