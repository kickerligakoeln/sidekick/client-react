import DumbComponent from "../../../js/DumbComponent";
import {ChannelSubscription, PubSub} from "../../../js/pubsub";
import Icon from "../Icon/Icon";

export default class HintActiveCheckins extends DumbComponent {

    mapChannel!: ChannelSubscription;

    constructor(props: any) {
        super(props);

        this.mapChannel = PubSub.join('#Map');

        this.state = {};
    }

    componentDidMount() {
        this.mapChannel.on('response-active-checkins', (checkins: any[]) => {
            this.setState({checkins});
        });
    }

    render() {
        if (!this.state.checkins) {
            return null;
        } else {
            return (
                <div className="btn-default">
                    <div className="indication-count">{this.state.checkins.length}</div>
                    <Icon icon={'map-marker-check'}/>
                </div>
            );
        }
    }
}