import React from 'react';
import ReactDOM from 'react-dom';
import "./js/utils/i18n";
import App from './App';
import {Helmet} from "react-helmet";
import {HashRouter} from "react-router-dom";
import reportWebVitals from './reportWebVitals';

import './css/base.scss';

declare var window: any;
declare var document: any;

ReactDOM.render(
        <HashRouter>
            <Helmet>
                <title>CheckIn | Kölner Kickerliga</title>
                <meta property="og:title" content="CheckIn! - Kölner Kickerliga"/>
                <meta property="og:url" content={window.location.origin}/>
                <meta property="og:description"
                      content="Lorem ipsum dolor"/>
                <meta property="og:type" content="website"/>
                <meta property="og:image" content=""/>
            </Helmet>
            <App/>
        </HashRouter>,
        document.getElementById('root')
);

reportWebVitals();
