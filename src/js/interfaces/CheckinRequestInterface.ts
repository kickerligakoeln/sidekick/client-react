import {VisibleState} from "../enums/VisibleState";
import {GeoInterface} from "./GeoInterface";

export interface CheckinRequestInterface {
    user?: {
        id: string|null
    }
    visible: VisibleState
    type: string
    coords: GeoInterface,
    location: {
        id: string
    }
}


export interface CheckinInterface {
    _id: {
        $oid: string
    }
    visible: VisibleState
    type: string
    coords: GeoInterface
    location: any
    create: string
    user: {
        id: string
    }
}
