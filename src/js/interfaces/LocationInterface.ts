export interface LocationInterface {
    name?: string
    url: string
    street?: string
    postalCode?: string
    city?: string
    longitude: number
    latitude: number
    distance: number
    lastUpdate?: string
    additionalInfo?: string
    website?: string
}
