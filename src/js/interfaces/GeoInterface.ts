export interface GeoInterface {
    accuracy?: number
    altitude?: number
    altitudeAccuracy?: number
    heading?: any
    latitude: number
    longitude: number
    speed?: number
}
