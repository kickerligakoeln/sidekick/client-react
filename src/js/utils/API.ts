import {merge} from "lodash";
import {Settings} from "./Settings";
import ApiRequestType from "../enums/ApiRequestType";

export default class API {

    API_URL = Settings.get('API_URL');

    /**
     *
     * @param method
     * @param uri
     * @param data
     * @param requestOptions
     * @returns {Promise<*>}
     */
    makeRequest(method: ApiRequestType, uri: string, data: any, requestOptions = {}) {
        return new Promise((resolve, reject) => {
            let url = this.API_URL + uri;

            let options = {
                'method': method.toString(),
                'headers': {
                    'Content-Type': 'application/json; charset=utf-8',
                }
            };

            merge(options,
                    requestOptions
            );

            if (data) {
                merge(options, {body: JSON.stringify(data)});
            }

            fetch(url, options)
                    .then(res => res.json())
                    .then(
                            (result) => resolve(result),
                            (error) => reject(error)
                    );
        });
    }

    /**
     *
     * @param uri
     * @param requestOptions
     * @param paramData
     * @returns {Q.Promise<any>}
     */
    get(uri: string, requestOptions?: any, paramData?: any): Promise<any> {
        let param: string = '';

        if (paramData) {
            param += '?';
            for (let key in paramData) {
                param += encodeURIComponent(key) + '=' + encodeURIComponent(paramData[key]) + '&';
            }
        }

        return this.makeRequest(ApiRequestType.GET, uri + param.slice(0, -1), null, requestOptions);
    }


    /**
     *
     * @param uri
     * @param data
     * @param requestOptions
     * @returns {Q.Promise<any>}
     */
    post(uri: string, data: any, requestOptions = {}) {
        return this.makeRequest(ApiRequestType.POST, uri, data, requestOptions);
    }


    /**
     *
     * @param uri
     * @param data
     * @param requestOptions
     * @returns {Q.Promise<any>}
     */
    put(uri: string, data: any, requestOptions = {}) {
        return this.makeRequest(ApiRequestType.PUT, uri, data, requestOptions);
    }


    /**
     *
     * @param uri
     * @param data
     * @param requestOptions
     * @returns {Q.Promise<any>}
     */
    delete(uri: string, data: any, requestOptions = {}) {
        return this.makeRequest(ApiRequestType.DELETE, uri, data, requestOptions);
    }


    /**
     *
     * @param uri
     * @param data
     * @param requestOptions
     * @returns {Q.Promise<any>}
     */
    patch(uri: string, data: any = {}, requestOptions = {}) {
        return this.makeRequest(ApiRequestType.PATCH, uri, data, requestOptions);
    }
}
