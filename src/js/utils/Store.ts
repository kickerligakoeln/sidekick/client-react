export default class Store {

    static shortname = 'sidekick';
    static devider = '.';


    /**
     *
     * @param c
     */
    static cookie(c: Cookie) {
        document.cookie = `${c.name}=${c.value}; expires=${c.expires}; path=${c.path};`;
    }


    /**
     * Cookie: get cookie (without prefix)
     * @param name
     */
    static getRawCookie(name: string) {
        const value: any = document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)');
        return value ? decodeURIComponent(value.pop()) : '';
    };


    /* ==== */


    /**
     * Cookie: get cookie
     * @param name
     * @returns {{}}
     */
    static getCookie(name: string) {
        return this.getRawCookie(this.shortname + this.devider + name);
    };


    /**
     * Cookie: set cookie
     * @param name
     * @param data
     * @param expireDateString
     */
    static setCookie(name: string, data: any, expireDateString = '') {
        let cookie: Cookie = {
            name: this.shortname + this.devider + name,
            value: (typeof data === 'string') ? data : JSON.stringify(data),
            path: '/',
            expires: new Date(expireDateString)
        };

        Store.cookie(cookie);
    };


    /**
     * Cookie: delete cookie
     * @param name
     */
    static removeCookie(name: string) {
        let cookie = {
            name: this.shortname + this.devider + name,
            value: "",
            path: '/',
            expires: new Date('Thu, 01 Jan 1970 00:00:00 UTC')
        };

        Store.cookie(cookie);
    }


    /**
     * LocalStorage: get item
     * @param name
     */
    static getLocalStorage(name: string) {
        try {
            let item: any = localStorage.getItem(this.shortname + this.devider + name);
            return JSON.parse(item);
        } catch (e) {
            return null;
        }
    }


    /**
     * LocalStorage: set item
     * @param name
     * @param data
     */
    static setLocalStorage(name: string, data: any) {
        let values = null;

        if (typeof data === 'string') {
            values = data;
        } else {
            values = JSON.stringify(data);
        }

        localStorage.setItem(this.shortname + this.devider + name, values);
    }


    /**
     * LocalStorage: delete item
     * @param name
     */
    static removeLocalStorage(name: string) {
        localStorage.removeItem(this.shortname + this.devider + name);
        return true;
    }


    /**
     * LocalStorage: clear
     */
    static clearLocalStorage() {
        localStorage.clear();
        return true;
    }


    /**
     * SessionStorage: get item
     * @param name
     */
    static getSessionStorage(name: string) {
        try {
            let item: any = sessionStorage.getItem(this.shortname + this.devider + name);
            return JSON.parse(item);
        } catch (e) {
            return false;
        }
    }


    /**
     * SessionStorage: set item
     * @param name
     * @param data
     */
    static setSessionStorage(name: string, data: any) {
        let values = null;

        if (typeof data === 'string') {
            values = data;
        } else {
            values = JSON.stringify(data);
        }

        sessionStorage.setItem(this.shortname + this.devider + name, values);
    }


    /**
     * SessionStorage: delete item
     * @param name
     */
    static removeSessionStorage(name: string) {
        sessionStorage.removeItem(this.shortname + this.devider + name);
        return true;
    }


    /**
     * SessionStorage: clear
     */
    static clearSessionStorage() {
        sessionStorage.clear();
        return true;
    }
}

export interface Cookie {
    name: string
    value: string,
    path: string,
    expires: Date
}