import Assert from "./Assert";

export default class URL {


    /**
     * get Url GET Parameter
     * {{ ?[name]=result }}
     *
     * @param name
     */
    static getUrlParam(name: string) {
        // eslint-disable-next-line
        let results: any = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (Assert.isUndefinedNullOrEmpty(results)) {
            return null;
        } else {
            return decodeURI(results[1]);
        }
    }
}
