import {GeoInterface} from "../interfaces/GeoInterface";

export default class Geo {

    /**
     *
     * @param g
     */
    static format(g: any): GeoInterface {
        return {
            accuracy: g.coords.accuracy,
            altitude: g.coords.altitude,
            altitudeAccuracy: g.coords.altitudeAccuracy,
            heading: g.coords.heading,
            latitude: g.coords.latitude,
            longitude: g.coords.longitude,
            speed: g.coords.speed
        }
    }

    /**
     * is feature available ?
     * @returns {Geolocation}
     */
    static available () {
        return (navigator.geolocation);
    }

    /**
     * get Current Position
     * @returns {Promise<unknown>}
     */
    static position(): Promise<GeoInterface> {
        return new Promise((resolve, reject) => {
            let options = {
                enableHighAccuracy: true,
                timeout: 1000,
                maximumAge: 0
            }

            navigator.geolocation.getCurrentPosition(
                (success) => resolve(Geo.format(success)),
                (error) => reject(error),
                options
            );
        });
    }
}
