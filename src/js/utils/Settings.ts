export const Settings = {


    /**
     *
     * @param key
     * @returns {string | null}
     */
    get: (key: string): string => {
        return process.env[`REACT_APP_` + key] || '';
    }
};
