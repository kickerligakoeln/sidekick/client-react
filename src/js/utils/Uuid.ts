/**
 * Generates a UUID v4
 *
 * http://stackoverflow.com/a/2117523/5779685
 * @returns {string} UUID v4
 */
export default function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        // eslint-disable-next-line
        let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
