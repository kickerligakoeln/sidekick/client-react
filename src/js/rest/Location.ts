import API from "../utils/API";
import {GeoInterface} from "../interfaces/GeoInterface";

export default class Location {

    static distance: number = 10; // kilometers

    static list = {
        /**
         * get recommendataions for current geo
         * @param coords
         */
        recommendations: (coords: GeoInterface): Promise<any[]> => {
            return new Promise((resolve, reject) => {
                const api = new API();

                const data = {
                    coords,
                    distance: Location.distance
                }

                api.post('/location/recommendations', data)
                    .then((response: any) => resolve(response))
                    .catch(e => reject(e));
            })
        }
    }
}
