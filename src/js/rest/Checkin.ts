import API from "../utils/API";
import {CheckinInterface, CheckinRequestInterface} from "../interfaces/CheckinRequestInterface";
import Store from "../utils/Store";

export default class Checkin {

    /**
     *
     * @param data
     */
    public static add(data: CheckinRequestInterface): Promise<CheckinInterface> {
        return new Promise((resolve, reject) => {
            const api = new API();

            // todo: read cookie in BE
            data['user'] = {
                id: Store.getCookie('access_token') || null
            }

            api.post('/checkin', data)
                .then((checkin: any) => {
                    Store.setCookie('access_token', checkin.user.id);
                    resolve(checkin);
                })
                .catch(e => reject(e));
        });
    }


    public static list = {

        /**
         * get active checkins
         */
        active: (): Promise<any> => {
            return new Promise<any>((resolve, reject) => {
                const api = new API();

                api.get('/checkin')
                    .then((response: any) => resolve(response))
                    .catch(e => reject(e));
            });
        }

    }

}
