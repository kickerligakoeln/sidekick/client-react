export enum VisibleState {
    PRIVATE = 'PRIVATE',
    PUBLIC = 'PUBLIC'
}