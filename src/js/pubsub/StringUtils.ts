import Assert from "../utils/Assert";

export default class StringUtils {

  /**
   * Search for `pattern` in `str` with glob support
   *
   * @example StringUtils.glob("hundkatzemaus", "*katze*") => true;
   * @param str Haystack
   * @param pattern Needle
   * @returns {boolean}
   */
  public static glob(str: string, pattern: string): boolean {
    if (Assert.isUndefinedNullOrEmpty(pattern)) {
      return false;
    }

    return new RegExp("^" + pattern.split("*").join(".*") + "$").test(str);
  }

  /**
   * [startsWith description]
   * @param  {string}  base         [description]
   * @param  {string}  searchString [description]
   * @param  {number}  position     [description]
   * @return {boolean}              [description]
   */
  public static startsWith(base: string, searchString: string, position?: number): boolean {
    position = position || 0;

    return base.indexOf(searchString, position) === position;
  }
}
