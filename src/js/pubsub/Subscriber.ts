export default interface Subscriber {
    id: string;
    callback: Function;
}
