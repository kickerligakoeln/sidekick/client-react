export default interface Message {
    id: string;
    data?: any[];
    sendAt?: number;
}
