import Channel from './Channel';
import Topic from './Topic';
import Message from './Message';
import URL from "../utils/URL";
import Assert from "../utils/Assert";
import _ from "lodash";

export default class ChannelSubscription {
    private name: string;
    private channel: Channel;

    constructor(name: string, channel: Channel) {
        this.name = name;
        this.channel = channel;
    }

    /**
     *
     * @param pattern
     * @param cb
     */
    public on(pattern: string, cb: Function): Map<String, String> {
        if (this.channel.isGlobSubscriber(pattern)) {
            this.channel.addGlobSubscriber(pattern, cb);
        } else {
            if (!this.channel.hasTopic(pattern)) {
                this.channel.createTopic(pattern);
            }
        }

        let subscriptionIds: any = {};
        this.channel.getTopics(pattern).forEach((t) => {
            subscriptionIds[t.name] = t.subscribe(cb);
        });

        return subscriptionIds;
    }

    /**
     *
     * @param subscription
     * @param pattern
     */
    public unsubscribe(subscription: any, pattern: string): void {
        if (!this.channel.hasTopic(pattern)) {
            return;
        }

        let sub = _.find(subscription, (s) => {
            return s[pattern];
        });

        if (!sub) {
            return;
        }

        this.channel.getTopics(pattern).forEach((t) => {
            t.unsubscribe(sub[pattern]);
        });
    }


    /**
     *
     * @param topicName
     * @param data
     */
    public send(topicName: string, ...data: any[]): void {
        this.deliver(false, topicName, ...data);
    }

    /**
     *
     * @param topicName
     * @param data
     */
    public sendSync(topicName: string, ...data: any[]): void {
        this.deliver(true, topicName, ...data);
    }

    /**
     *
     * @param deliverSync
     * @param topicName
     * @param data
     */
    private deliver(deliverSync: boolean, topicName: string, ...data: any[]): void {
        let topic: Topic | any = this.channel.getTopic(topicName);

        if (Assert.isUndefinedNullOrEmpty(topic)) {
            topic = this.channel.createTopic(topicName);
        }

        if (URL.getUrlParam('log')) {
            console.log(`%c [PubSub] ${this.channel.name} ${this.name}: [${topic.name}]: `, 'background: #222; color: #70cc41', data);
        }

        if (deliverSync) {
            topic.publishSync(...data);
        } else {
            topic.publish(...data);
        }
    }

    /**
     *
     * @param topic
     * @returns {{[topicName: string]: Message[]}}
     */
    public getHistory(topic: string): { [topicName: string]: Message[] } {
        const history: { [topicName: string]: Message[] } = {};

        console.info("Topics with pattern:", topic, this.channel.getTopics(topic));

        this.channel.getTopics(topic).forEach((t) => {
            if (!history[t.name]) {
                history[t.name] = [];
            }

            history[t.name] = t.getHistory();
        });

        return history;
    }
}
