db.checkin.insertMany([
    {
        "visible": "PUBLIC",
        "type": "other",
        "location": {
            "id": "hammond-bar-koeln-suedstadt",
        },
        "coords": {
            "longitude": 6.9544425,
            "latitude": 50.9207612
        },
        "create": "2021-03-29T19:40:40+02:00",
        "user": {
            "id": "160621118a4733"
        }
    },
    {
        "visible": "PUBLIC",
        "type": "other",
        "location": {
            "id": "koelner-kickerfabrik-e-v-koeln-suedstadt",
        },
        "coords": {
            "longitude": 6.955491899999999,
            "latitude": 50.9231851
        },
        "create": "2021-03-29T19:40:40+02:00",
        "user": {
            "id": "160621118a4123"
        }
    },
    {
        "visible": "PUBLIC",
        "type": "other",
        "location": {
            "id": "lotta-koeln-suedstadt"
        },
        "coords": {
            "longitude": 6.9583477,
            "latitude": 50.9222391
        },
        "create": "2021-03-29T19:40:40+02:00",
        "user": {
            "id": "160621118a4349"
        }
    },
    {
        "visible": "PUBLIC",
        "type": "other",
        "location": {
            "id": "limes-koeln-muelheim",
        },
        "coords": {
            "longitude": 7.0022606,
            "latitude": 50.9672256
        },
        "create": "2021-03-29T19:40:40+02:00",
        "user": {
            "id": "160621118a4671"
        }
    },
    {
        "visible": "PUBLIC",
        "type": "other",
        "location": {
            "id": "quirls-bergisch-gladbach",
        },
        "coords": {
            "longitude": 7.1356765,
            "latitude": 50.99153940000001
        },
        "create": "2021-03-29T19:40:40+02:00",
        "user": {
            "id": "160621118a4733"
        }
    }
])