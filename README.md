# Sidekick
A web application for sharing the current location based data.


## Requirements

**React** \
[![npm package](https://img.shields.io/npm/v/react)](https://www.npmjs.com/package/react)

**Typescript** \
[![npm package](https://img.shields.io/npm/v/typescript)](https://www.npmjs.com/package/typescript)

**@react-google-maps/api** \
[![npm package](https://img.shields.io/npm/v/@react-google-maps/api)](https://www.npmjs.com/package/@react-google-maps/api)

---

## Initial

```bash
# Set up the environment file
> cp ./docs/example.env .env

# build application
> nvm use
> yarn install
```


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
